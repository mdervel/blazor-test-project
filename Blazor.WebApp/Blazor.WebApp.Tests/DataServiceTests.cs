using Currencies.WebAPI.Services;
using Xunit;

namespace Blazor.WebApp.Tests
{
    public class DataServiceTests
    {
        [Fact]
        public void FetchDataTest()
        {
            var dataService = new DataService();
            var data = dataService.FetchData().Result;
            Assert.True(data != null);
            Assert.Equal(30,data.Data.Length);
        }
    }
}
