﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Currencies.Core
{
    public class CurrenciesResponse
    {
        [JsonProperty("data")]
        public CurrenciesDataItem[] Data { get; set; }
    }

    public class CurrenciesDataItem
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }


        [JsonProperty("quote")]
        public Quote Quote { get; set; }
    }

    public class Quote
    {
        public USDInfo USD { get; set; }
    }

    public class USDInfo
    {
        [JsonProperty("price")]
        public float Price { get; set; }

        [JsonProperty("percent_change_1h")]
        public float Prc1h { get; set; }

        [JsonProperty("percent_change_24h")]
        public float Prc24h { get; set; }

        [JsonProperty("last_updated")]
        public DateTime LastUpdated { get; set; }

        [JsonProperty("market_cap")]
        public float MarketCap { get; set; }
    }
}
