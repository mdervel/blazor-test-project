﻿using Currencies.Core;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Currencies.WebAPI.Services
{
    public interface IDataService
    {
        Task<CurrenciesResponse> FetchData();
    }

    public class DataService : IDataService
    {
        private readonly HttpClient _httpClient;

        public DataService()
        {
            _httpClient = new HttpClient();
        }

        public async Task<CurrenciesResponse> FetchData()
        {
            var url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?start=1&limit=30&convert=USD&sort=market_cap&sort_dir=desc";
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.DefaultRequestHeaders.Add("X-CMC_PRO_API_KEY", "c73c740e-ef3d-4655-8c82-0a8fdad784b3");
            var response = await _httpClient.GetStringAsync(url);
            return JsonConvert.DeserializeObject<CurrenciesResponse>(response);
        }
    }
}
