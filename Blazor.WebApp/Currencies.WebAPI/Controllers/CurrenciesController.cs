﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Currencies.WebAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace Currencies.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class CurrenciesController : Controller
    {
        private readonly IDataService _dataService;

        public CurrenciesController(IDataService dataService)
        {
            _dataService = dataService;
        }
        [HttpGet]
        public async Task<JsonResult> Get()
        {
            var data = await _dataService.FetchData();
            return Json(data);
        }

    }
}
