﻿function GetFavoritesKey() {
    return "favorites";
}

function AddToFavorites(currencies) {
    if (!currencies) {
        currencies = [];
    }
    window.localStorage.setItem(GetFavoritesKey(), JSON.stringify(currencies));
}

function GetFavorites() {
    var value = window.localStorage.getItem(GetFavoritesKey());
    if (value) {
        return JSON.parse(window.localStorage.getItem(GetFavoritesKey()));
    }
    return [];
}