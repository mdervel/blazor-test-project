﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Blazor.WebApp.Models;
using Microsoft.AspNetCore.Components;
using Currencies.Core;
using AutoMapper;


namespace Blazor.WebApp.Services
{
    public interface IDataService
    {
        Task<CurrencyViewModel[]> FetchData();
    }

    public class DataService : IDataService
    {
        private readonly HttpClient _httpClient;
        private readonly IMapper _mapper;

        public DataService(HttpClient httpClient, IMapper mapper)
        {
            _httpClient = httpClient;
            _mapper = mapper;
        }

        public async Task<CurrencyViewModel[]> FetchData()
        {
            var url = "http://localhost:50397/api/currencies";
            var response = await _httpClient.GetJsonAsync<CurrenciesResponse>(url);

            if (response?.Data.Length > 0)
            {
                return _mapper.Map<CurrencyViewModel[]>(response.Data);
            }
            return null;
        }
    }
}
