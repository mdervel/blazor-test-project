﻿using Currencies.Core;
using Blazor.WebApp.Models;
using AutoMapper;
using System;

namespace Blazor.WebApp.Automap
{
    public class WebAppProfile : Profile
    {
        public WebAppProfile()
        {
            CreateMap<CurrenciesDataItem, CurrencyViewModel>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dst => dst.Price, opt => opt.MapFrom(src => src.Quote.USD.Price))
                .ForMember(dst => dst.Prc24h, opt => opt.MapFrom(src => src.Quote.USD.Prc24h))
                .ForMember(dst => dst.Prc1h, opt => opt.MapFrom(src => src.Quote.USD.Prc1h))
                .ForMember(dst => dst.MarketCap, opt => opt.MapFrom(src => src.Quote.USD.MarketCap))
                .ForMember(dst => dst.LastUpdated, opt => opt.MapFrom(src => DateTime.Now.ToString("g")))
                .ForAllOtherMembers(opt=>opt.Ignore());
        }
    }
}
