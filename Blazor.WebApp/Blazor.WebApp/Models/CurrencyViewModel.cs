﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blazor.WebApp.Models
{
    public class CurrencyViewModel
    {
        public long Id { get; set; }

        public int OrderNum { get; set; }

        public string Name { get; set; }

        public string Icon => $"https://s2.coinmarketcap.com/static/img/coins/32x32/{Id}.png";

        public float MarketCap { get; set; }

        public float Price { get; set; }

        public float Prc1h { get; set; }

        public float Prc24h { get; set; }

        public string LastUpdated { get; set; }

        public bool IsChecked { get; set; }

        public bool IsFavorite { get; set; }
    }
}
